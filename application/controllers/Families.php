<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/18/2017
 * Time: 11:47 AM
 */

class Families extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('person_model');
    }

    public function index()
    {
        $group_id = $this->session->userdata('group_id');

        $data['families'] = $this->person_model->select("last")->group_by('last')->get(['group_id' => $group_id])->all();

        $this->load->view('header');
        $this->load->view('navbar', ['nav' => 2]);
        $this->load->view('families', $data);
        $this->load->view('footer');
    }
}