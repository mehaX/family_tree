<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 17-Apr-17
 * Time: 9:19 PM
 */
class Groups extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('groups_model');
    }

    public function change_group($group_id = null)
    {
        if (!$group_id || ($group = $this->groups_model->get_group($group_id)) == null)
            show_404();

        $this->session->set_userdata(array(
            'group_id'  => $group['id'],
            'group_name'=> $group['name']
        ));

        redirect('');
    }
}