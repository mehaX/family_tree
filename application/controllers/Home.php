<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 7/3/2016
 * Time: 6:46 PM
 */
class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('groups_model');
        $this->load->model('family_model');

        $this->load->helper('url');

        //$this->output->enable_profiler(true);
    }

    public function index($person_id = null)
    {
        if ($person_id == null)
            $person = $this->family_model->get_first_person();
        else
            $person = $this->family_model->get_person($person_id);

        if (!$person)
            show_404();

        $person_id          = $person['id'];

        $data               = $this->family_model->get_family($person_id);
        $data['person_id']  = $person_id;
        $groups             = $this->groups_model->get_all();

        $this->load->view('header');
        $this->load->view('navbar', array('groups' => $groups));
        $this->load->view('index', $data);
        $this->load->view('footer');
    }

    public function parent($person_id = 1)
    {
        $id = $this->input->post('id');
        $gender = $this->input->post('gender');

        $parent = array(
            'first'     => $this->input->post('first'),
            'last'      => $this->input->post('last')
        );

        if (empty($id))
            echo json_encode($this->family_model->insert_parents_and_child($parent, $gender, $person_id));
        else
            $this->family_model->update_person($parent, $id);
    }

    public function sibling($person_id = 1)
    {
        $child_id = $this->input->post('id');
        $relation_id = $this->input->post('relation_id');
        $person = array(
            'first'     => $this->input->post('first'),
            'last'      => $this->input->post('last'),
            'gender'    => $this->input->post('gender')
        );

        if (empty($child_id))
        {
            $child_id = $this->family_model->insert_person($person);

            if (empty($relation_id))
            {
                $result = $this->family_model->insert_parents_and_child(array(), 1, $child_id);
                $this->family_model->insert_child($result['relation_id'], $person_id);
            }
            else
                $this->family_model->insert_child($relation_id, $child_id);
        }
        else
            $this->family_model->update_person($person, $child_id);
    }

    public function child($person_id = 1)
    {
        $child_id = $this->input->post('id');
        $relation_id = $this->input->post('relation_id');
        $person = array(
            'first'     => $this->input->post('first'),
            'last'      => $this->input->post('last'),
            'gender'    => $this->input->post('gender')
        );

        if (empty($child_id))
        {
            $child_id = $this->family_model->insert_person($person);

            if (empty($relation_id))
                $relation_id = $this->family_model->insert_new_relation($person_id);

            $this->family_model->insert_child($relation_id, $child_id);
        }
        else
            $this->family_model->update_person($person, $child_id);
    }

    public function spouse($person_id = 1)
    {
        $spouse_id = $this->input->post('id');

        $person = array(
            'first'     => $this->input->post('first'),
            'last'      => $this->input->post('last')
        );

        if (empty($spouse_id))
        {
            $you = $this->family_model->get_person($person_id);
            $spouse_gender = ($you['gender'] + 1)%2;
            $person['gender'] = $spouse_gender;

            $spouse_id = $this->family_model->insert_person($person);
            $this->family_model->insert_new_relation($person_id, $spouse_id, $spouse_gender);
        }
        else
            $this->family_model->update_person($person, $spouse_id);
    }
}