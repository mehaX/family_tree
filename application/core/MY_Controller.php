<?php
/**
 * Created by PhpStorm.
 * User: losi_
 * Date: 8/18/2017
 * Time: 11:56 AM
 */

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->database();

        $this->load->library('session');

        $this->load->helper('url');

        if (!$this->session->userdata('group_id'))
        {
            $group = $this->db->limit(1)->get('groups')->row_array();
            $this->session->set_userdata(array(
                'group_id'      => $group['id'],
                'group_name'    => $group['name']
            ));
        }

        if (!$this->input->is_ajax_request())
        {
            $this->load->model('group_model');

            $data['groups'] = $this->group_model->get()->all();
            $this->load->vars($data);
        }
    }
}