<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 7/3/2016
 * Time: 7:28 PM
 */
class Family_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_family($person_id)
    {
        //////// You
        $data['you']    = $this->db->get_where('people', array('id' => $person_id))->row_array();

        $your_gender    = ($data['you']['gender'] == 0 ? 'female' : 'male').'_id';
        $spouse_gender  = ($data['you']['gender'] == 1 ? 'female' : 'male').'_id';


        //////// Parents
        $this->db->select("c.id, c.relation_id, father.id as 'father_id', father.first as 'father_first', father.middle as 'father_middle', father.last as 'father_last', mother.id as 'mother_id', mother.first as 'mother_first', mother.middle as 'mother_middle', mother.last as 'mother_last'");
        $this->db->from('children c');
        $this->db->join('relations r', 'c.relation_id=r.id', 'left');
        $this->db->join('people father', 'r.male_id=father.id', 'left');
        $this->db->join('people mother', 'r.female_id=mother.id', 'left');
        $this->db->where('c.child_id', $person_id);
        $data['parents']    = $this->db->get()->row_array();

        $father_id          = $data['parents']['father_id'];
        $mother_id          = $data['parents']['mother_id'];

        //////// Siblings
        if ($data['parents'] != null)
        {
            $this->db->select("c.relation_id, p.id, p.first, p.middle, p.last, p.gender, p.birthday");
            $this->db->from('children c');
            $this->db->join('relations r', 'r.id=c.relation_id');
            $this->db->join('people p', 'p.id=c.child_id');
            $this->db->where("(r.male_id=$father_id OR r.female_id=$mother_id)");
            $this->db->where('c.child_id!=', $person_id);
            $data['siblings'] = $this->db->get()->result_array();
        }
        else
            $data['siblings'] = array();


        //////// Spouse (relations)
        $this->db->select("p.*, r.id as 'relation_id'");
        $this->db->from('relations r');
        $this->db->join('people p', "p.id=r.$spouse_gender", 'left');
        $this->db->where("r.$your_gender", $person_id);
        $data['spouses'] = $this->db->get()->result_array();

        //////// Children
        $children = "
            select p.*, c.relation_id
            from children c
            join people p
            on p.id=c.child_id
        ";

        $this->db->select("c.first, c.middle, c.last, c.gender, c.birthday, c.relation_id, c.id as 'child_id', r.$spouse_gender");
        $this->db->from("relations r");
        $this->db->join("($children) c", "c.relation_id=r.id");
        $this->db->where("r.$your_gender", $person_id);
        $this->db->order_by('c.id', 'asc');
        $data['children'] = $this->db->get()->result_array();

        return $data;
    }

    public function get_person($person_id)
    {
        return $this->db->get_where('people', array('id' => $person_id))->row_array();
    }

    public function get_first_person()
    {
        $gropu_id = $this->session->userdata('group_id');
        return $this->db->where('group_id', $gropu_id)->limit(1)->order_by('id', 'asc')->get('people')->row_array();
    }

    public function insert_person($person)
    {
        $this->load->model('groups_model');

        if (empty($person['first']))
            unset($person['first']);
        $person['group_id'] = $this->session->userdata('group_id');
        $this->db->insert('people', $person);
        return $this->db->insert_id();
    }

    public function update_person($person, $person_id)
    {
        $this->db->update('people', $person, array('id' => $person_id));
    }

    public function insert_parents($parent, $gender)
    {
        $parent['gender'] = $gender;
        $parent_id = $this->insert_person($parent);

        $parent2 = array('gender' => ($gender+1) % 2);
        $parent2_id = $this->insert_person($parent2);

        if ($gender == 0)
        {
            $mother_id  = $parent_id;
            $father_id  = $parent2_id;
        }
        else
        {
            $father_id = $parent_id;
            $mother_id = $parent2_id;
        }

        return array(
            'father_id' => $father_id,
            'mother_id' => $mother_id
        );
    }

    public function insert_relation($parents_id)
    {
        $this->db->insert('relations', array('male_id' => $parents_id['father_id'], 'female_id' => $parents_id['mother_id']));
        return $this->db->insert_id();
    }

    public function insert_new_relation($person1_id, $person2_id = null, $spouse_gender = null)
    {
        $person1 = $this->db->get_where('people', array('id' => $person1_id))->row_array();

        if ($person2_id == null){
            $spouse_gender = ($person1['gender']+1)%2;
            $this->db->insert('people', array('gender' => $spouse_gender));
            $person2_id = $this->db->insert_id();
        }

        if ($spouse_gender == 0)
        {
            echo "no";
            $male = $person1_id;
            $female = $person2_id;
        }
        else
        {
            echo "yes";
            $male = $person2_id;
            $female = $person1_id;
        }

        $this->db->insert('relations', array('male_id' => $male, 'female_id' => $female));
        return $this->db->insert_id();
    }

    public function insert_child($relation_id, $child_id)
    {
        $this->db->insert('children', array('relation_id' => $relation_id, 'child_id' => $child_id));
        return $this->db->insert_id();
    }

    public function insert_parents_and_child($parent, $gender, $child_id)
    {
        $parents_id     = $this->insert_parents($parent, $gender);
        $relation_id    = $this->insert_relation($parents_id);
        $child_id       = $this->insert_child($relation_id, $child_id);

        return array_merge($parents_id, array('relation_id' => $relation_id, 'child_id' => $child_id));
    }
}