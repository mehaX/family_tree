<?php

/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 17-Apr-17
 * Time: 9:09 PM
 */
class Groups_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_all()
    {
        return $this->db->get('groups')->result_array();
    }

    public function get_group($group_id)
    {
        return $this->db->get_where('groups', array('id' => $group_id))->row_array();
    }
}