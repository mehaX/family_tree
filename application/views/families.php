<div class="container">
    <div class="col-xs-12 col-md-8 col-lg-6">
        <div class="list-group">
            <?php foreach ($families as $family): ?>
                <a href="#" class="list-group-item">
                    <h4><?php echo $family['last'] ?></h4>
                </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>