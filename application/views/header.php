<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 7/3/2016
 * Time: 7:15 PM
 */
?>
<!DOCTYPE html>
<html>
<head>
    <title>Family tree</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

    <script src="<?php echo base_url(); ?>assets/js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>path.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/global.js"></script>
</head>
<body>
