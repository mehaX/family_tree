<?php
/**
 * Created by PhpStorm.
 * User: mehaX
 * Date: 7/3/2016
 * Time: 7:12 PM
 */
?>
<div class="container content" data-yourid="<?php echo $person_id; ?>">
    <div class="row">
        <div class="col-md-offset-2 col-md-4 col-xs-6">
            <div class="panel panel-primary panel-father-js" data-type="father" data-id="<?php echo $parents['father_id'] ?>" data-gender="1">
                <div class="panel-heading">
                    <h3 class="panel-title">Father</h3>
                </div>

                <div class="panel-body">
                    <span class="panel-first-js"><?php echo $parents['father_first']; ?></span> <span class="panel-last-js"><?php echo $parents['father_last']; ?></span>
                </div>

                <div class="panel-footer">
                    <?php if ($parents['father_id']): ?><a class="btn btn-raised btn-sm btn-info" href="<?php echo base_url(array('person', $parents['father_id'])); ?>"><span class="glyphicon glyphicon-search"></span> <span class="hidden-xs">View</span></a><?php endif; ?>
                    <button class="btn btn-raised btn-sm btn-<?php echo ($parents['father_id'] ? 'warning' : 'success'); ?> btn-parent-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-<?php echo ($parents['father_id'] ? 'edit' : 'plus'); ?>"></span> <span class="hidden-xs"><?php echo ($parents['father_id'] ? 'Edit' : 'Add'); ?></span></button>
                </div>
            </div>
        </div>

        <div class="col-md-4 col-xs-6">
            <div class="panel panel-danger panel-mother-js" data-type="mother" data-id="<?php echo $parents['mother_id'] ?>" data-gender="0">
                <div class="panel-heading">
                    <h3 class="panel-title">Mother</h3>
                </div>

                <div class="panel-body">
                    <span class="panel-first-js"><?php echo $parents['mother_first']; ?></span> <span class="panel-last-js"><?php echo $parents['mother_last']; ?></span>
                </div>

                <div class="panel-footer">
                    <?php if ($parents['mother_id']): ?><a class="btn btn-raised btn-sm btn-info" href="<?php echo base_url(array('person', $parents['mother_id'])); ?>"><span class="glyphicon glyphicon-search"></span> <span class="hidden-xs">View</span></a><?php endif; ?>
                    <button class="btn btn-raised btn-sm btn-<?php echo ($parents['father_id'] ? 'warning' : 'success'); ?> btn-parent-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-<?php echo ($parents['father_id'] ? 'edit' : 'plus'); ?>"></span> <span class="hidden-xs"><?php echo ($parents['mother_id'] ? 'Edit' : 'Add'); ?></span></button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default panel-siblings-js" data-id="<?php echo $parents['relation_id']; ?>">
                <div class="panel-heading">
                    <h3 class="panel-title">Siblings</h3>
                </div>

                <ul class="list-group">
                    <?php foreach ($siblings as  $sibling): ?>
                        <li class="list-group-item list-group-item-<?php echo ($sibling['gender'] == 1 ? 'info' : 'danger'); ?>" data-id="<?php echo $sibling['id']; ?>" data-gender="<?php echo $sibling['gender']; ?>">
                            <span class="first-js"><?php echo $sibling['first']; ?></span> <span class="last-js"><?php echo $sibling['last']; ?></span>
                            <div class="btn-group pull-right">
                                <a href="<?php echo base_url(array('person', $sibling['id'])); ?>" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-search"></span></a>
                                <button class="btn btn-xs btn-warning edit-sibling-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-edit"></span></button>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <div class="panel-footer">
                    <button class="btn btn-raised btn-sm btn-success add-sibling-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-plus"></span> Add</button>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-<?php echo ($you['gender'] == 1 ? 'primary' : 'danger'); ?>">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $you['first'].' '.$you['last']; ?></h3>
                </div>

                <div class="panel-body">

                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="panel panel-<?php echo ($you['gender'] == 0 ? 'primary' : 'danger'); ?> panel-spouse-js">
                <div class="panel-heading">
                    <h3 class="panel-title">Spouse / Other relations</h3>
                </div>

                <ul class="list-group" data-id="<?php if (!empty($spouses)) echo $spouses[0]['relation_id']; ?>">
                    <?php foreach ($spouses as $spouse): ?>
                    <li class="list-group-item spouse-item-js" id="relation-<?php echo $spouse['relation_id']; ?>" data-id="<?php echo $spouse['id']; ?>" data-gender="<?php echo $spouse['gender']; ?>">
                        <span class="first-js"><?php echo $spouse['first']; ?></span> <span class="last-js"><?php echo $spouse['last']; ?></span>
                        <div class="btn-group pull-right">
                            <?php if (count($spouses) > 1): ?><button class="btn btn-xs btn-default select-spouse-js"><span class="glyphicon glyphicon-ok"></span></button><?php endif; ?>
                            <a href="<?php echo base_url(array('person', $spouse['id'])); ?>" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-search"></span></a>
                            <button class="btn btn-xs btn-warning edit-spouse-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-edit"></span></button>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>

                <div class="panel-footer">
                    <button class="btn btn-raised btn-sm btn-success add-spouse-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-plus"></span> Add</button>
                </div>
            </div>

            <div class="panel panel-default panel-children-js">
                <div class="panel-heading">
                    <h3 class="panel-title">Children</h3>
                </div>

                <ul class="list-group">
                    <?php foreach ($children as  $child): ?>
                    <li class="list-group-item list-group-item-<?php echo ($child['gender'] == 1 ? 'info' : 'danger'); ?>" data-relation_id="<?php echo $child['relation_id']; ?>" data-id="<?php echo $child['child_id']; ?>" data-gender="<?php echo $child['gender']; ?>">
                        <span class="first-js"><?php echo $child['first']; ?></span> <span class="last-js"><?php echo $child['last']; ?></span>
                        <div class="btn-group pull-right">
                            <a href="<?php echo base_url(array('person', $child['child_id'])); ?>" class="btn btn-xs btn-info"><span class="glyphicon glyphicon-search"></span></a>
                            <button class="btn btn-xs btn-warning edit-child-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-edit"></span></button>
                        </div>
                    </li>
                    <?php endforeach; ?>
                </ul>

                <div class="panel-footer">
                    <button class="btn btn-raised btn-sm btn-success add-child-js" data-toggle="modal" data-target="#personModal"><span class="glyphicon glyphicon-plus"></span> Add</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="personModal" tabindex="-1" role="dialog" data-id="" data-type="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-2 control-label">First</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" id="modal-first-js">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-2 control-label">Last</label>
                        <div class="col-xs-10">
                            <input type="text" class="form-control" id="modal-last-js">
                        </div>
                    </div>

                    <div class="form-group group-gender-js">
                        <label class="col-xs-2 control-label">Gender</label>
                        <div class="col-xs-10">
                            <select class="form-control" id="modal-gender-js">
                                <option value="1" selected>Male</option>
                                <option value="0">Female</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-raised btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-raised btn-primary modal-submit-js">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->