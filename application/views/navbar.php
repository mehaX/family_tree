<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand active" href="<?php echo base_url(); ?>">Family tree</a>
            <button class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li <?php if (!isset($nav) || $nav == 1): ?>class="active"<?php endif ?>><a href="<?php echo base_url() ?>">Home</a></li>
                <li <?php if (isset($nav) && $nav == 2): ?>class="active"<?php endif ?>><a href="<?php echo base_url('families') ?>">Families</a></li>
            </ul>

            <ul class="nav navbar-nav pull-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> <?php echo $this->session->userdata('group_name'); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php foreach ($groups as $group): ?>
                        <li><a class="force" href="<?php echo base_url(array('groups', 'change_group', $group['id'])); ?>"><?php echo $group['name']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>