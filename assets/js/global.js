/*$(function () {
    $('.datepicker-js').datetimepicker({
        format: 'DD/MM/YYYY',
        viewMode: 'years'
    });
});*/

$(document)

.on('click', '.btn-parent-js', function(){
    var panel   = $(this).closest('.panel');
    var type    = panel.data('type');
    var id      = panel.data('id');

    var first   = panel.find('.panel-first-js').html();
    var last    = panel.find('.panel-last-js').html();
    var gender  = panel.data('gender');

    $('#personModal').data('type', type);
    $('#personModal').data('id', id);
    $('#modal-first-js').val(first);
    $('#modal-last-js').val(last);
    $('#modal-gender-js').val(gender);

    $('.group-gender-js').addClass('hidden');

    $('.modal-submit-js').attr('id', 'parent-submit-js');
})

.on('click', '#parent-submit-js', function(){
    var first   = $('#modal-first-js').val();
    var last    = $('#modal-last-js').val();
    var gender  = $('#modal-gender-js').val();

    var id      = $('#personModal').data('id');
    var panel   = $('.panel-' + (gender == 1 ? 'father' : 'mother') + '-js');

    panel.find('.panel-first-js').html(first);
    panel.find('.panel-last-js').html(last);

    var your_id = $('.container.content').data('yourid');

    $.ajax({
        type: 'post',
        url: path + 'parent_submit/' + your_id,
        data: {id: id, first: first, last: last, gender: gender},
        success: function(html){
            location.reload();
        }
    });
})

.on('click', '.add-sibling-js', function(){

    $('#personModal').data('type', 'sibling');
    $('#personModal').data('id', '');
    $('#modal-first-js').val('');
    $('#modal-last-js').val('');
    $('#modal-gender-js').val(1);

    $('.group-gender-js').removeClass('hidden');
    $('.modal-submit-js').attr('id', 'sibling-submit-js');
})
.on('click', '.edit-sibling-js', function(){
    var row = $(this).closest('.list-group-item');

    var id      = row.data('id');
    var gender  = row.data('gender');
    var first   = row.find('.first-js').html();
    var last    = row.find('.last-js').html();

    $('#personModal').data('type', 'sibling');
    $('#personModal').data('id', id);
    $('#modal-first-js').val(first);
    $('#modal-last-js').val(last);
    $('#modal-gender-js').val(gender);

    $('.group-gender-js').removeClass('hidden');
    $('.modal-submit-js').attr('id', 'sibling-submit-js');
})

.on('click', '#sibling-submit-js', function(){
    var your_id     = $('.container.content').data('yourid');

    var id          = $('#personModal').data('id');
    var first       = $('#modal-first-js').val();
    var last        = $('#modal-last-js').val();
    var gender      = $('#modal-gender-js').val();

    var panel       = $('.panel-siblings-js');
    var relation_id = panel.data('id');

    $.ajax({
        type: 'post',
        url: path + 'sibling_submit/' + your_id,
        data: {id: id, relation_id: relation_id, first: first, last: last, gender: gender},
        success: function(html){
            location.reload();
        }
    });
})

.on('click', '.select-spouse-js', function(){
    var li = $(this).closest('.list-group-item');
    var relation_id = li.attr('id').replace('relation-', '');
    var ul = li.closest('.list-group');
    ul.data('id', relation_id);
    ul.find('.active').removeClass('active');
    li.addClass('active');

    $('.panel-children-js .list-group-item').each(function(){
        if ($(this).data('relation_id') == relation_id)
            $(this).fadeIn();
        else
            $(this).fadeOut();
    })
})

.on('click', '.add-child-js', function(){
    $('#personModal').data('type', 'child');
    $('#personModal').data('id', '');
    $('#modal-first-js').val('');
    $('#modal-last-js').val('');
    $('#modal-gender-js').val(1);

    $('.group-gender-js').removeClass('hidden');
    $('.modal-submit-js').attr('id', 'child-submit-js');
})
.on('click', '.edit-child-js', function(){
    var row = $(this).closest('.list-group-item');

    var id      = row.data('id');
    var gender  = row.data('gender');
    var first   = row.find('.first-js').html();
    var last    = row.find('.last-js').html();

    $('#personModal').data('type', 'child');
    $('#personModal').data('id', id);
    $('#modal-first-js').val(first);
    $('#modal-last-js').val(last);
    $('#modal-gender-js').val(gender);

    $('.group-gender-js').removeClass('hidden');
    $('.modal-submit-js').attr('id', 'child-submit-js');
})

.on('click', '#child-submit-js', function(){
    var your_id     = $('.container.content').data('yourid');

    var id          = $('#personModal').data('id');
    var first       = $('#modal-first-js').val();
    var last        = $('#modal-last-js').val();
    var gender      = $('#modal-gender-js').val();

    var panel       = $('.panel-spouse-js .list-group');
    var relation_id = panel.data('id');

    $.ajax({
        type: 'post',
        url: path + 'child_submit/' + your_id,
        data: {id: id, relation_id: relation_id, first: first, last: last, gender: gender},
        success: function(html){
            location.reload();
        }
    });
})


.on('click', '.add-spouse-js', function(){
    $('#personModal').data('type', 'spouse');
    $('#personModal').data('id', '');
    $('#modal-first-js').val('');
    $('#modal-last-js').val('');
    $('#modal-gender-js').val(1);

    $('.group-gender-js').addClass('hidden');
    $('.modal-submit-js').attr('id', 'spouse-submit-js');
})
.on('click', '.edit-spouse-js', function(){
    var row = $(this).closest('.list-group-item');

    var id      = row.data('id');
    var gender  = row.data('gender');
    var first   = row.find('.first-js').html();
    var last    = row.find('.last-js').html();

    $('#personModal').data('type', 'spouse');
    $('#personModal').data('id', id);
    $('#modal-first-js').val(first);
    $('#modal-last-js').val(last);
    $('#modal-gender-js').val(gender);

    $('.group-gender-js').addClass('hidden');
    $('.modal-submit-js').attr('id', 'spouse-submit-js');
})
.on('click', '#spouse-submit-js', function(){
    var your_id     = $('.container.content').data('yourid');

    var id          = $('#personModal').data('id');
    var first       = $('#modal-first-js').val();
    var last        = $('#modal-last-js').val();
    var gender      = $('#modal-gender-js').val();

    var panel       = $('.panel-spouse-js .select-spouse-js');
    var relation_id = panel.data('id');

    $.ajax({
        type: 'post',
        url: path + 'spouse_submit/' + your_id,
        data: {id: id, relation_id: relation_id, first: first, last: last, gender: gender},
        success: function(html){
            location.reload();
        }
    });
})